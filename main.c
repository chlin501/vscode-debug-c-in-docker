#include <stdio.h>
#include <string.h>
#include <stdint.h>

struct person 
{
  char name[125];
  int age;
  uint8_t sex; 
};

void process(struct person *person)
{
  printf("name: %s\n", person->name);
  printf("age: %d\n", person->age);
  printf("gender: %d\n", person->sex);

}

int main()
{
  struct person person;
  printf("Program gets started! ...\n");
  memset(&person, 0, sizeof(person));
  strncpy(person.name, "John Smith", 10); 
  person.age = 32;
  person.sex = 1;
  process(&person);
  printf("Done! ...\n");
  return 0;
}
