# vscode-debug-c-docker

# Prerequiste

* GNU/ Debian: trixie/sid

* vscode: 1.82.6

* docker: 20.10.25

## Note

Unfortunately, vscodium and podman does NOT work at all.

# Steps

* Create binary: `make all`

* Build image: `./build-docker`

* Run docker: `./run-docker`

* Start debugging: Click vscode *Run and Debug* icon

# Troubleshooting

* sshpass exits code 6

This is because `Host public key is unknown. sshpass exits without confirming the new key`. Please login once that will create a new key by `ssh root@localhost -p 2222`

* version `GLIBC_2.34' not found

Docker based image needs to be at testing `FROM debian:testing` (28-02-2024).

# References

[1]. [#VSCode: C++ Development and Debugging using containers](https://lemariva.com/blog/2020/10/vscode-c-development-and-debugging-containers)
